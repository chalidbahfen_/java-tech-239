package com.xsis.JavaBootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBc239Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaBc239Application.class, args);
	}

}
